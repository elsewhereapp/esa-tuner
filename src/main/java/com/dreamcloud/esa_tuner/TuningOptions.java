package com.dreamcloud.esa_tuner;

import java.io.File;

public class TuningOptions {
    private String action = "tune";
    private String type = "spearman";
    private int startingWindowSize;
    private int endingWindowSize;
    private int windowSizeStep;
    private float startingWindowDrop;
    private float endingWindowDrop;
    private float windowDropStep;
    private int startingVectorLimit;
    private int endingVectorLimit;
    private int vectorLimitStep;
    private boolean debugMode = false;
    private File outputFile;
    private String printVectorText;
    private File scoreCompareSpearmanCsv1;
    private File scoreCompareSpearmanCsv2;
    private File word2VecFile;
    private String scoreCompareTerm;
    private String strategy = "esa"; //esa, word2vec, combined

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStartingWindowSize() {
        return startingWindowSize;
    }

    public void setStartingWindowSize(int startingWindowSize) {
        this.startingWindowSize = startingWindowSize;
    }

    public int getEndingWindowSize() {
        return endingWindowSize;
    }

    public void setEndingWindowSize(int endingWindowSize) {
        this.endingWindowSize = endingWindowSize;
    }

    public int getWindowSizeStep() {
        return windowSizeStep;
    }

    public void setWindowSizeStep(int windowSizeStep) {
        this.windowSizeStep = windowSizeStep;
    }

    public float getStartingWindowDrop() {
        return startingWindowDrop;
    }

    public void setStartingWindowDrop(float startingWindowDrop) {
        this.startingWindowDrop = startingWindowDrop;
    }

    public float getEndingWindowDrop() {
        return endingWindowDrop;
    }

    public void setEndingWindowDrop(float endingWindowDrop) {
        this.endingWindowDrop = endingWindowDrop;
    }

    public float getWindowDropStep() {
        return windowDropStep;
    }

    public void setWindowDropStep(float windowDropStep) {
        this.windowDropStep = windowDropStep;
    }

    public int getStartingVectorLimit() {
        return startingVectorLimit;
    }

    public void setStartingVectorLimit(int startingVectorLimit) {
        this.startingVectorLimit = startingVectorLimit;
    }

    public int getEndingVectorLimit() {
        return endingVectorLimit;
    }

    public void setEndingVectorLimit(int endingVectorLimit) {
        this.endingVectorLimit = endingVectorLimit;
    }

    public int getVectorLimitStep() {
        return vectorLimitStep;
    }

    public void setVectorLimitStep(int vectorLimitStep) {
        this.vectorLimitStep = vectorLimitStep;
    }

    public void setOutputFile(File file) {
        this.outputFile = outputFile;
    }
    
    public File getOutputFile(File file) {
        return outputFile;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public File getOutputFile() {
        return outputFile;
    }

    public String getPrintVectorText() {
        return printVectorText;
    }

    public void setPrintVectorText(String printVectorText) {
        this.printVectorText = printVectorText;
    }

    public File getScoreCompareSpearmanCsv1() {
        return scoreCompareSpearmanCsv1;
    }

    public void setScoreCompareSpearmanCsv1(File scoreCompareSpearmanCsv1) {
        this.scoreCompareSpearmanCsv1 = scoreCompareSpearmanCsv1;
    }

    public File getScoreCompareSpearmanCsv2() {
        return scoreCompareSpearmanCsv2;
    }

    public void setScoreCompareSpearmanCsv2(File scoreCompareSpearmanCsv2) {
        this.scoreCompareSpearmanCsv2 = scoreCompareSpearmanCsv2;
    }

    public String getScoreCompareTerm() {
        return scoreCompareTerm;
    }

    public void setScoreCompareTerm(String scoreCompareTerm) {
        this.scoreCompareTerm = scoreCompareTerm;
    }

    public boolean isDebugMode() {
        return debugMode;
    }

    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
    }

    public File getWord2VecFile() {
        return word2VecFile;
    }

    public void setWord2VecFile(File word2VecFile) {
        this.word2VecFile = word2VecFile;
    }

    public void setTuneStrategy(String strategy) {
        this.strategy = strategy;;
    }

    public String getTuneStrategy() {
        return strategy;
    }
}
