package com.dreamcloud.esa_tuner;

import com.dreamcloud.esa_core.similarity.DocumentSimilarity;
import com.dreamcloud.esa_core.similarity.TextSimilarity;
import com.opencsv.exceptions.CsvValidationException;
import org.apache.commons.math.stat.correlation.PearsonsCorrelation;
import org.apache.commons.math.stat.correlation.SpearmansCorrelation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class PValueCalculator {
    private boolean outputWordPairs = true;
    private final File file;
    private final File documentFile;
    private ArrayList<DocumentPair> humanSimilarityList;

    public PValueCalculator(File file, File documentFile) {
        this.file = file;
        this.documentFile = documentFile;
    }

    public PValueCalculator(File file) {
        this(file, null);
    }

    public void setOutputWordPairs(boolean outputWordPairs) {
        this.outputWordPairs = outputWordPairs;
    }

    /*public double getPearsonCorrelation(DocumentSimilarity similarity) throws Exception {
        ArrayList<DocumentPair> humanSimilarityList = this.readHumanScores();
        ArrayList<DocumentPair> esaScoreList = this.getEsaScores(humanSimilarityList, similarity);
        return this.getPearsonCorrelation(humanSimilarityList, esaScoreList);
    }*/

    public double getPearsonCorrelation(ArrayList<DocumentPair> docs1, ArrayList<DocumentPair> docs2) {
        double[] doc1Scores = docs1.stream().mapToDouble(DocumentPair::getScore).toArray();
        double[] doc2Scores = docs2.stream().mapToDouble(DocumentPair::getScore).toArray();
        return new PearsonsCorrelation().correlation(doc1Scores, doc2Scores);
    }

    /*public double getSpearmanCorrelation(DocumentSimilarity similarity) throws Exception {
        ArrayList<DocumentPair> humanSimilarityList = this.readHumanScores();
        ArrayList<DocumentPair> esaScoreList = this.getEsaScores(humanSimilarityList, similarity);
        return this.getSpearmanCorrelation(humanSimilarityList, esaScoreList);
    }*/

    public double getSpearmanCorrelation(ArrayList<DocumentPair> docs1, ArrayList<DocumentPair> docs2) {
        double[] doc1Scores = docs1.stream().mapToDouble(DocumentPair::getScore).toArray();
        double[] doc2Scores = docs2.stream().mapToDouble(DocumentPair::getScore).toArray();

        return new SpearmansCorrelation().correlation(doc1Scores, doc2Scores);
    }

    public ArrayList<DocumentPair> getEsaScores(ArrayList<DocumentPair> humanSimilarityList, TextSimilarity similarity) throws Exception {
        ArrayList<DocumentPair> normalizedScores = new ArrayList<>();
        for(int i=0; i<humanSimilarityList.size(); i++) {
            DocumentPair docSim = humanSimilarityList.get(i);
            float score = similarity.score(docSim.getDoc1(), docSim.getDoc2()).getScore();
            normalizedScores.add(new DocumentPair(docSim.getDoc1(), docSim.getDoc2(), score));

            String sourceDesc = docSim.getDoc1().substring(0, Math.min(16, docSim.getDoc1().length()));
            String compareDesc = docSim.getDoc2().substring(0, Math.min(16, docSim.getDoc2().length()));

            if (outputWordPairs) {
                System.out.println("doc " + i + "\t ('" + sourceDesc + "', '" + compareDesc + "'):\t" + score);
            }
        }

        return normalizedScores;
    }

    /*public ArrayList<DocumentPair> getEsaScores(ArrayList<DocumentPair> humanSimilarityList, TextSimilarity similarity1, TextSimilarity similarity2) throws Exception {
        ArrayList<DocumentPair> esaScoreList1 = new ArrayList<>();
        for(int i=0; i<humanSimilarityList.size(); i++) {
            DocumentPair docSim = humanSimilarityList.get(i);
            float score = similarity1.score(docSim.getDoc1(), docSim.getDoc2()).getScore();
            esaScoreList1.add(new DocumentPair(docSim.getDoc1(), docSim.getDoc2(), score));

            String sourceDesc = docSim.getDoc1().substring(0, Math.min(16, docSim.getDoc1().length()));
            String compareDesc = docSim.getDoc2().substring(0, Math.min(16, docSim.getDoc2().length()));

            if (outputWordPairs) {
                System.out.println("doc " + i + "\t ('" + sourceDesc + "', '" + compareDesc + "'):\t" + score);
            }
        }

        ArrayList<DocumentPair> esaScoreList2 = new ArrayList<>();
        for(int i=0; i<humanSimilarityList.size(); i++) {
            DocumentPair docSim = humanSimilarityList.get(i);
            float score = similarity2.score(docSim.getDoc1(), docSim.getDoc2()).getScore();
            esaScoreList2.add(new DocumentPair(docSim.getDoc1(), docSim.getDoc2(), score));

            String sourceDesc = docSim.getDoc1().substring(0, Math.min(16, docSim.getDoc1().length()));
            String compareDesc = docSim.getDoc2().substring(0, Math.min(16, docSim.getDoc2().length()));

            if (outputWordPairs) {
                System.out.println("doc " + i + "\t ('" + sourceDesc + "', '" + compareDesc + "'):\t" + score);
            }
        }

        //Create sorted lists
        List<Float> sortedScores1 = new ArrayList<>();
        List<Float> sortedScores2 = new ArrayList<>();
        for (int i=0; i<humanSimilarityList.size(); i++) {
            sortedScores1.add(esaScoreList1.get(i).getScore());
            sortedScores2.add(esaScoreList2.get(i).getScore());
        }
        sortedScores1.sort((Float f1, Float f2) -> Float.compare(f1, f2));
        sortedScores2.sort((Float f1, Float f2) -> Float.compare(f1, f2));

        ArrayList<DocumentPair> normalizedScores = new ArrayList<>();
        for (int i=0; i<humanSimilarityList.size(); i++) {
            float score1 = sortedScores1.indexOf(esaScoreList1.get(i).getScore());
            float score2 = sortedScores2.indexOf(esaScoreList2.get(i).getScore());
            if (Float.isNaN(score1) && Float.isNaN(score2)) {
                score1 = score2 = 0;
            } else if(Float.isNaN(score1)) {
                score1 = score2;
            } else if(Float.isNaN(score2)) {
                score2 = score1;
            }
            score1 *= 1;
            score2 *= 1;
            float normalizedRank = (score1 + score2) / 2.0f;
            normalizedScores.add(new DocumentPair(humanSimilarityList.get(i).getDoc1(), humanSimilarityList.get(i).getDoc2(), normalizedRank));
        }

        return normalizedScores;
    }*/

    public ArrayList<DocumentPair> readHumanScores() throws IOException, CsvValidationException {
        if (humanSimilarityList == null) {
            humanSimilarityList = DocumentPairCsvReader.readDocumentPairs(file, documentFile);
        }
        return humanSimilarityList;
    }
}
