package com.dreamcloud.esa_tuner;

import com.dreamcloud.esa_core.similarity.TextSimilarity;
import com.dreamcloud.esa_core.vectorizer.VectorBuilder;
import com.dreamcloud.esa_core.vectorizer.scoreMod.PruneScoreMod;
import com.dreamcloud.esa_core.vectorizer.scoreMod.VectorLimitScoreMod;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class PrunerTuner {
    protected TextSimilarity similarity;

    public PrunerTuner(TextSimilarity similarity) {
        this.similarity = similarity;
    }

    double verify(PValueCalculator calculator, TuningOptions options) throws Exception {
        //Load the human scores once
        ArrayList<DocumentPair> humanScores = calculator.readHumanScores();
        ArrayList<DocumentPair> esaScores = calculator.getEsaScores(humanScores, similarity);
        if ("pearson".equals(options.getType())) {
            return calculator.getPearsonCorrelation(esaScores, humanScores);
        } else {
            return calculator.getSpearmanCorrelation(esaScores, humanScores);
        }
    }


    public PrunerTuning tune(PValueCalculator pValueCalculator, TuningOptions options, VectorLimitScoreMod vectorLimitScoreMod, PruneScoreMod pruneScoreMod) throws Exception {
        AtomicBoolean skipWindow = new AtomicBoolean(false);
        Thread skipThread = new Thread(() -> {
            while (!Thread.interrupted()) {
                Scanner scanner = new Scanner(System.in);
                String line = scanner.nextLine();
                if ("s".equals(line)) {
                    System.out.println("skipping (user)");
                    skipWindow.set(true);
                }
            }
        });
        skipThread.setDaemon(true);
        skipThread.start();
        float initialDropOffStart = options.getStartingWindowDrop();

        int windowStart = options.getStartingWindowSize();
        int windowEnd = options.getEndingWindowSize();
        int windowStep = options.getWindowSizeStep();

        float dropOffStart = options.getStartingWindowDrop();
        float dropOffEnd = options.getEndingWindowDrop();
        float dropOffStep = options.getWindowDropStep();

        int vectorLimitStart = options.getStartingVectorLimit();
        int vectorLimitEnd = options.getEndingVectorLimit();

        NumberFormat format = NumberFormat.getInstance();
        format.setMaximumFractionDigits(8);
        format.setMinimumFractionDigits(8);

        double bestScore = 0;
        int bestWindowSize = 0;
        float bestDropOff = 0;
        float bestVectorLimit = 0;
        ArrayList<DocumentPair> bestEsaScores = null;

        //Load the human scores once
        ArrayList<DocumentPair> humanScores = pValueCalculator.readHumanScores();

        //Tuning the window size and dropoff
        if (windowStart != windowEnd || dropOffStart != dropOffEnd) {
            int iterationIdx = 0;
            int iterationCount = (((windowEnd - windowStart) / windowStep) + 1) * (int) (((dropOffEnd - dropOffStart) / dropOffStep) + 1);

            while (windowStart <= windowEnd) {
                dropOffStart = initialDropOffStart;
                ArrayList<Double> lastScores = new ArrayList<>();
                while (dropOffStart <= dropOffEnd) {
                    if (skipWindow.get()) {
                        skipWindow.set(false);
                        break;
                    }

                    //Change prune options (same object as in the pvalue calculator!)
                    pruneScoreMod.setWindowSize(windowStart);
                    pruneScoreMod.setWindowDrop(dropOffStart);
                    //hacky shmack
                    VectorBuilder.cache.clear();


                    double score;
                    ArrayList<DocumentPair> esaScores = pValueCalculator.getEsaScores(humanScores, similarity);
                    if ("pearson".equals(options.getType())) {
                        score = pValueCalculator.getPearsonCorrelation(esaScores, humanScores);
                    } else {
                        score = pValueCalculator.getSpearmanCorrelation(esaScores, humanScores);
                    }
                    lastScores.add(score);
                    if (lastScores.size() > 10) {
                        lastScores.remove(0);
                    }

                    if (score > bestScore) {
                        bestScore = score;
                        bestWindowSize = windowStart;
                        bestDropOff = dropOffStart;
                        bestEsaScores = esaScores;
                        System.out.println("!!! best score !!!");
                    }

                    //If we have 10 scores, check to see if 7 of the 10 are lower than the previous ones
                    if (lastScores.size() == 10) {
                        int lowerScores = 0;
                        for (int scoreIdx = 1; scoreIdx < 10; scoreIdx++) {
                            double currentScore = lastScores.get(scoreIdx);
                            double previousScore = lastScores.get(scoreIdx - 1);
                            if (currentScore < previousScore) {
                                lowerScores++;
                            }
                        }
                        if (lowerScores >= 8) {
                            System.out.println("skipping (9)");
                            iterationIdx += (int) (((dropOffEnd - dropOffStart) / dropOffStep) + 1);
                            break;
                        }
                    }

                    System.out.println(format.format(score) + ": " + windowStart + "/" + format.format(dropOffStart) + "\t[" + iterationIdx + "|" + iterationCount + "]\tbest: " + format.format(bestScore));
                    dropOffStart += dropOffStep;
                    iterationIdx++;
                }
                windowStart += windowStep;
            }
        } else if(vectorLimitStart != vectorLimitEnd) {
            //Tuning the vector limit
            while (vectorLimitStart <= vectorLimitEnd) {
                vectorLimitScoreMod.setMaxLength(vectorLimitStart);
                VectorBuilder.cache.clear();
                double score;
                ArrayList<DocumentPair> esaScores = pValueCalculator.getEsaScores(humanScores, similarity);
                if ("pearson".equals(options.getType())) {
                    score = pValueCalculator.getPearsonCorrelation(esaScores, humanScores);
                } else {
                    score = pValueCalculator.getSpearmanCorrelation(esaScores, humanScores);
                }
                if (score > bestScore) {
                    bestScore = score;
                    bestVectorLimit = vectorLimitStart;
                    bestEsaScores = esaScores;
                    System.out.println("!!! best score !!!");
                }
                System.out.println(format.format(score) + "\t[" + vectorLimitStart + " / " + vectorLimitEnd + "]\tbest: " + format.format(bestScore));
               vectorLimitStart += options.getVectorLimitStep();
            }
        } else {
            System.out.println("warning: nothing to tune");
        }

        return new PrunerTuning(bestScore, bestWindowSize, bestDropOff, bestVectorLimit, bestEsaScores);
    }
}
