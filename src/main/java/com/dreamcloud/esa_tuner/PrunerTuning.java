package com.dreamcloud.esa_tuner;

import java.util.ArrayList;

public class PrunerTuning {
    private final double tunedScore;
    private final int tunedWindowSize;
    private final double tunedWindowDropOff;
    private final double tunedVectorLimit;
    private final ArrayList<DocumentPair> tunedEsaScores;

    public PrunerTuning(double tunedScore, int tunedWindowSize, double tunedWindowDropOff, double tunedVectorLimit, ArrayList<DocumentPair> tunedEsaScores) {
        this.tunedScore = tunedScore;
        this.tunedWindowSize = tunedWindowSize;
        this.tunedWindowDropOff = tunedWindowDropOff;
        this.tunedVectorLimit = tunedVectorLimit;
        this.tunedEsaScores = tunedEsaScores;
    }

    public double getTunedScore() {
        return tunedScore;
    }

    public double getTunedWindowDropOff() {
        return tunedWindowDropOff;
    }

    public int getTunedWindowSize() {
        return tunedWindowSize;
    }

    public double getTunedVectorLimit() {
        return tunedVectorLimit;
    }

    public ArrayList<DocumentPair> getTunedEsaScores() {
        return tunedEsaScores;
    }
}
