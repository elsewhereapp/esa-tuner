package com.dreamcloud.esa_tuner.cli;

import com.dreamcloud.esa_config.cli.OptionsReader;
import com.dreamcloud.esa_tuner.TuningOptions;

import java.io.File;
import java.util.Properties;

public class TuningOptionsReader extends OptionsReader {
    private static String TUNE_ACTION = "tuning.action";
    private static String DEBUG_MODE = "tuning.debug";

    private static String TUNE_TYPE = "tuning.tune.type";
    private static String TUNE_STRATEGY = "tuning.tune.strategy";
    private static String WORD_2_VEC_SOURCE = "tuning.tune.word2vec.source";
    private static String TUNE_OUTPUT = "tuning.tune.output";
    private static String WINDOW_SIZE_START = "tuning.tune.windowStart";
    private static String WINDOW_SIZE_END = "tuning.tune.windowEnd";
    private static String WINDOW_SIZE_STEP = "tuning.tune.windowStep";
    private static String WINDOW_DROP_START = "tuning.tune.dropOffStart";
    private static String WINDOW_DROP_END = "tuning.tune.dropOffEnd";
    private static String WINDOW_DROP_STEP = "tuning.tune.dropOffStep";
    private static String VECTOR_LIMIT_START = "tuning.tune.vectorLimitStart";
    private static String VECTOR_LIMIT_END = "tuning.tune.vectorLimitEnd";
    private static String VECTOR_LIMIT_STEP = "tuning.tune.vectorLimitStep";

    private static String PRINT_VECTOR_TEXT = "tuning.print-vector.text";

    private static String SCORE_COMPARE_SPEARMAN_1 = "tuning.score-compare.spearmanCsv1";
    private static String SCORE_COMPARE_SPEARMAN_2 = "tuning.score-compare.spearmanCsv2";
    private static String SCORE_COMPARE_TERM = "tuning.score-compare.term";

    public TuningOptions getOptions(Properties properties) {
        TuningOptions options = new TuningOptions();

        if (hasProperty(properties, TUNE_ACTION)) {
            options.setAction(properties.getProperty(TUNE_ACTION));
        }

        if (hasProperty(properties, TUNE_STRATEGY)) {
            options.setTuneStrategy(properties.getProperty(TUNE_STRATEGY));
        }

        if (isEnabled(properties, DEBUG_MODE)) {
            options.setDebugMode(true);
        }


        if (hasProperty(properties, PRINT_VECTOR_TEXT)) {
            options.setPrintVectorText(properties.getProperty(PRINT_VECTOR_TEXT));
        }

        if (hasProperty(properties, SCORE_COMPARE_SPEARMAN_1)) {
            options.setScoreCompareSpearmanCsv1(new File(properties.getProperty(SCORE_COMPARE_SPEARMAN_1)));
        }
        if (hasProperty(properties, SCORE_COMPARE_SPEARMAN_2)) {
            options.setScoreCompareSpearmanCsv2(new File(properties.getProperty(SCORE_COMPARE_SPEARMAN_2)));
        }
        if (hasProperty(properties, SCORE_COMPARE_TERM)) {
            options.setScoreCompareTerm(properties.getProperty(SCORE_COMPARE_TERM));
        }

        if (hasProperty(properties, TUNE_TYPE)) {
            options.setType(properties.getProperty(TUNE_TYPE));
        }

        //Window size
        if (hasProperty(properties, WINDOW_SIZE_START)) {
            options.setStartingWindowSize(Integer.parseInt(properties.getProperty(WINDOW_SIZE_START)));
        }
        if (hasProperty(properties, WINDOW_SIZE_END)) {
            options.setEndingWindowSize(Integer.parseInt(properties.getProperty(WINDOW_SIZE_END)));
        }
        if (hasProperty(properties, WINDOW_SIZE_STEP)) {
            options.setWindowSizeStep(Integer.parseInt(properties.getProperty(WINDOW_SIZE_STEP)));
        }

        //Window drop
        if (hasProperty(properties, WINDOW_DROP_START)) {
            options.setStartingWindowDrop(Float.parseFloat(properties.getProperty(WINDOW_DROP_START)));
        }
        if (hasProperty(properties, WINDOW_DROP_END)) {
            options.setEndingWindowDrop(Float.parseFloat(properties.getProperty(WINDOW_DROP_END)));
        }
        if (hasProperty(properties, WINDOW_DROP_STEP)) {
            options.setWindowDropStep(Float.parseFloat(properties.getProperty(WINDOW_DROP_STEP)));
        }

        //Vector limit
        if (hasProperty(properties, VECTOR_LIMIT_START)) {
            options.setStartingVectorLimit(Integer.parseInt(properties.getProperty(VECTOR_LIMIT_START)));
        }
        if (hasProperty(properties, VECTOR_LIMIT_END)) {
            options.setEndingVectorLimit(Integer.parseInt(properties.getProperty(VECTOR_LIMIT_END)));
        }
        if (hasProperty(properties, VECTOR_LIMIT_STEP)) {
            options.setVectorLimitStep(Integer.parseInt(properties.getProperty(VECTOR_LIMIT_STEP)));
        }

        if (hasProperty(properties, WORD_2_VEC_SOURCE)) {
            options.setWord2VecFile(new File(properties.getProperty(WORD_2_VEC_SOURCE)));
        }

        if (hasProperty(properties, TUNE_OUTPUT)) {
            options.setOutputFile(new File(properties.getProperty(TUNE_OUTPUT)));
        }

        return options;
    }
}
