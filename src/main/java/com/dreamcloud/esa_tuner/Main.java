package com.dreamcloud.esa_tuner;

import com.dreamcloud.esa_config.cli.AnalyzerOptionsReader;
import com.dreamcloud.esa_config.cli.CollectionOptionsReader;
import com.dreamcloud.esa_config.cli.TfIdfOptionsReader;
import com.dreamcloud.esa_config.cli.VectorizationOptionsReader;
import com.dreamcloud.esa_core.analyzer.AnalyzerOptions;
import com.dreamcloud.esa_core.analyzer.EsaAnalyzer;
import com.dreamcloud.esa_core.analyzer.TokenizerFactory;
import com.dreamcloud.esa_core.similarity.DocumentSimilarity;
import com.dreamcloud.esa_core.similarity.TextSimilarity;
import com.dreamcloud.esa_core.vectorizer.*;
import com.dreamcloud.esa_core.vectorizer.scoreMod.PruneScoreMod;
import com.dreamcloud.esa_core.vectorizer.scoreMod.VectorLimitScoreMod;
import com.dreamcloud.esa_score.analysis.TfIdfAnalyzer;
import com.dreamcloud.esa_score.analysis.TfIdfOptions;
import com.dreamcloud.esa_score.analysis.TfIdfStrategyFactory;
import com.dreamcloud.esa_score.analysis.strategy.TfIdfStrategy;
import com.dreamcloud.esa_score.fs.*;
import com.dreamcloud.esa_score.score.DocumentNameResolver;
import com.dreamcloud.esa_score.score.TfIdfScore;
import com.dreamcloud.esa_tuner.cli.TuningOptionsReader;

import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        //Set up the ESA properties
        String propertiesPath =  "./esa.properties";
        Properties properties = new Properties();
        properties.load(new FileInputStream(propertiesPath));

        System.out.println("Properties");
        System.out.println("----------------------------------------");
        properties.stringPropertyNames().stream().sorted().forEach((String property) -> {
            System.out.println(property + "\t\t=  " + properties.getProperty(property));
        });
        System.out.println("----------------------------------------\n");

        VectorizationOptions vectorOptions = new VectorizationOptionsReader().getOptions(properties);
        AnalyzerOptions analyzerOptions = new AnalyzerOptionsReader().getOptions(properties);
        TfIdfOptions tfIdfOptions = new TfIdfOptionsReader().getOptions(properties);
        CollectionOptions collectionOptions = new CollectionOptionsReader().getOptions(properties);
        TuningOptions tuningOptions = new TuningOptionsReader().getOptions(properties);

        analyzerOptions.setTokenizerFactory(new TokenizerFactory() {
            public Tokenizer getTokenizer() {
                return new StandardTokenizer();
            }
        });


        File spearmanFile = new File("./src/data/en-wordsim353.csv");
        File pearsonFile = new File("./src/data/en-lp50.csv");
        File documentFile = new File("./src/data/en-lp50-documents.csv");

        try {
            TfIdfStrategyFactory tfIdfFactory = new TfIdfStrategyFactory();
            TfIdfStrategy tfIdfStrategy = tfIdfFactory.getStrategy(tfIdfOptions);

            TfIdfAnalyzer tfIdfAnalyzer = new TfIdfAnalyzer(tfIdfStrategy, new EsaAnalyzer(analyzerOptions), collectionOptions.getTermIndex());
            VectorBuilder vectorBuilder = new VectorBuilder(collectionOptions.getScoreReader(), tfIdfAnalyzer, analyzerOptions.getPreprocessor(), vectorOptions);

            PruneScoreMod pruneScoreMod = new PruneScoreMod(vectorOptions.getWindowSize(), vectorOptions.getWindowDrop());
            VectorLimitScoreMod vectorLimitScoreMod = new VectorLimitScoreMod(vectorOptions.getVectorLimit());
            vectorBuilder.addScoreMod(pruneScoreMod);
            vectorBuilder.addScoreMod(vectorLimitScoreMod);

            DocumentNameResolver nameResolver = collectionOptions.getDocumentNameResolver();
            TextVectorizer textVectorizer = new Vectorizer(vectorBuilder);

            switch (tuningOptions.getAction()) {
                case "tune":
                    PValueCalculator pValueCalculator;
                    if ("pearson".equals(tuningOptions.getType())) {
                        pValueCalculator = new PValueCalculator(pearsonFile, documentFile);
                    } else {
                        pValueCalculator = new PValueCalculator(spearmanFile);
                    }
                    pValueCalculator.setOutputWordPairs(tuningOptions.isDebugMode());
                    TextSimilarity similarity = new DocumentSimilarity(textVectorizer);
                    PrunerTuner tuner = new PrunerTuner(similarity);
                    System.out.println("Analyzing wordsim-353 to find the ideal vector limit...");
                    System.out.println("----------------------------------------");
                    PrunerTuning tuning = tuner.tune(pValueCalculator, tuningOptions, vectorLimitScoreMod, pruneScoreMod);
                    System.out.println("tuned p-value:\t" + tuning.getTunedScore());
                    System.out.println("tuned window size:\t" + tuning.getTunedWindowSize());
                    System.out.println("tuned window dropoff:\t" + tuning.getTunedWindowDropOff());
                    System.out.println("tuned vector limit:\t" + tuning.getTunedVectorLimit());

                    if (tuningOptions.getOutputFile() != null && tuning.getTunedEsaScores() != null) {
                        DocumentPairCsvWriter csvWriter = new DocumentPairCsvWriter(tuningOptions.getOutputFile());
                        csvWriter.writePairs(tuning.getTunedEsaScores());
                    }
                    System.out.println("----------------------------------------");
                    break;
                case "verify":
                    //Runs a verification test on the ESA index using Spearman and Pearson
                    PValueCalculator spearmanCalculator = new PValueCalculator(spearmanFile);
                    spearmanCalculator.setOutputWordPairs(tuningOptions.isDebugMode());
                    PValueCalculator pearsonCalculator = new PValueCalculator(pearsonFile, documentFile);
                    pearsonCalculator.setOutputWordPairs(tuningOptions.isDebugMode());

                    TextSimilarity verifySimilarity = new DocumentSimilarity(textVectorizer);
                    PrunerTuner verifyTuner = new PrunerTuner(verifySimilarity);

                    System.out.println("Analyzing index with Spearman (WordSim-353)...");
                    System.out.println("----------------------------------------");
                    double spearmanPValue = verifyTuner.verify(spearmanCalculator, tuningOptions);
                    System.out.println("p-value:\t" + spearmanPValue);
                    System.out.println("----------------------------------------\n");

                    System.out.println("Analyzing index with Pearson (LP50)...");
                    System.out.println("----------------------------------------");
                    double pearsonPValue = verifyTuner.verify(pearsonCalculator, tuningOptions);
                    System.out.println("p-value:\t" + pearsonPValue);
                    System.out.println("----------------------------------------");
                    break;
                case "print-vector":
                    DocumentScoreVector vector = textVectorizer.vectorize(tuningOptions.getPrintVectorText());
                    Vector<TfIdfScore> scores = new Vector<>();
                    for (Integer documentId : vector.getDocumentScores().keySet()) {
                        scores.add(new TfIdfScore(documentId, null, vector.getScore(documentId)));
                    }
                    scores.sort((TfIdfScore s1, TfIdfScore s2) -> Double.compare(s2.getScore(), s1.getScore()));
                    for (TfIdfScore score : scores) {
                        System.out.println((nameResolver != null ? nameResolver.getTitle(score.getDocument()) : score.getDocument()) + ":\t" + score.getScore());
                    }
                    break;
                case "score-compare":
                    //Compare two spearman correlations to analyze the difference
                    File spearman1 = tuningOptions.getScoreCompareSpearmanCsv1();
                    File spearman2 = tuningOptions.getScoreCompareSpearmanCsv2();

                    ArrayList<DocumentPair> spearman1Scores = DocumentPairCsvReader.readDocumentPairs(spearman1);
                    ArrayList<DocumentPair> spearman2Scores = DocumentPairCsvReader.readDocumentPairs(spearman2);

                    SpearmanComparison spearmanComparison = new SpearmanComparison(spearman1Scores, spearman2Scores);
                    Map<String, Integer> sortedScoreDiffs = spearmanComparison.getDocsByRankDiff();
                    for (String wordPair : sortedScoreDiffs.keySet()) {
                        System.out.println(wordPair + ": " + sortedScoreDiffs.get(wordPair));
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Invalid tuning action: " + tuningOptions.getAction());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
